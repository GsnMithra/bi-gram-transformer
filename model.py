import math
from typing import Optional, Tuple

import torch
import torch.nn as nn
from torch.nn import functional as F
from tqdm import tqdm

device = torch.device("mps" if torch.backends.mps.is_available() else "cpu")


class Head(nn.Module):
    """Self-Attention Head"""

    def __init__(self, n_embd, n_head, head_size):
        super().__init__()
        self.query = nn.Linear(n_embd, head_size, bias=False, device=device)
        self.key = nn.Linear(n_embd, head_size, bias=False, device=device)
        self.value = nn.Linear(n_embd, head_size, bias=False, device=device)

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        B, T, C = input.shape
        Q = self.query(input)
        K = self.key(input)

        attn = Q @ K.transpose(-2, -1)
        attn = attn / math.sqrt(C)
        attn = F.softmax(attn, dim=-1)
        out = self.value(input)

        out = attn @ out
        return out


class MultiHeadAttention(nn.Module):
    """Multi-Head Attention Block"""

    def __init__(self, n_embd, n_head):
        super().__init__()
        self.heads = nn.ModuleList(
            [Head(n_embd, n_head, n_embd // n_head) for _ in range(n_head)]
        )
        self.linear = nn.Linear(n_embd, n_embd, device=device)

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        out = torch.cat([head(input) for head in self.heads], dim=-1)
        out = self.linear(out)
        return out


class FeedForward(nn.Module):
    """Feed Forward Network"""

    def __init__(self, n_embd):
        super().__init__()
        self.network = nn.Sequential(
            nn.Linear(n_embd, 4 * n_embd, device=device),
            nn.Linear(4 * n_embd, 10 * n_embd, device=device),
            nn.Linear(10 * n_embd, 4 * n_embd, device=device),
            nn.Linear(4 * n_embd, n_embd, device=device),
        )

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        out = self.network(input)
        return out


class Block(nn.Module):
    """Single block in a layer"""

    def __init__(self, n_embd, n_head):
        super().__init__()
        self.mha = MultiHeadAttention(n_embd, n_head)
        self.ffn = FeedForward(n_embd)
        self.normX = nn.LayerNorm(n_embd, device=device)
        self.normY = nn.LayerNorm(n_embd, device=device)

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        out_mha = self.mha(input)
        out_norm = self.normX(out_mha + input)
        out_ffn = self.ffn(out_norm)
        out = self.normY(out_norm + out_ffn)
        return out


class Transformer(nn.Module):
    """Transformer Unit"""

    def __init__(self, vocab_size, n_embd, n_head, n_layers, block_size, batch_size):
        super().__init__()
        self.to(device)
        self.block_size, self.batch_size = (block_size, batch_size)
        self.inp_encds = nn.Embedding(vocab_size, n_embd, device=device)
        self.pos_encds = nn.Embedding(block_size, n_embd, device=device)
        self.layers = nn.Sequential(*[Block(n_embd, n_head) for _ in range(n_layers)])
        self.norm = nn.LayerNorm(n_embd, device=device)
        self.linear = nn.Linear(n_embd, vocab_size, device=device)

    def forward(
        self, input: torch.Tensor, output=None
    ) -> Tuple[torch.Tensor, Optional[torch.Tensor]]:
        B, T = input.shape
        input_embd = self.inp_encds(input)
        pos_embd = self.pos_encds(torch.arange(T, dtype=torch.long, device=device))
        transformer_input = input_embd + pos_embd
        out = self.layers(transformer_input)
        out = self.norm(out)

        if output is None:
            loss = None
        else:
            X, Y, Z = out.shape
            out_transformer = out.view(X * Y, Z)
            output = output.view(X * Y)
            loss = F.cross_entropy(out_transformer, output)

        return (out, loss)

    def generate(
        self,
        input_token=torch.zeros(1, 1, dtype=torch.long, device=device),
        max_tokens=3000,
    ) -> torch.Tensor:
        for t in range(max_tokens):
            input = input_token[:, -self.block_size :]
            out, loss = self(input)
            out = out[:, -1, :]
            out = F.softmax(out, dim=-1)
            next = torch.multinomial(out, num_samples=1)
            input_token = torch.cat((input_token, next), dim=-1)
        return input_token

    def __fetch_batch(self, dataset: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        indices = torch.randint(len(dataset) - self.block_size, (self.batch_size,))
        X = torch.stack([dataset[idx : idx + self.block_size] for idx in indices])
        yhat = torch.stack(
            [dataset[idx + 1 : idx + self.block_size + 1] for idx in indices]
        )
        return (X.to(device), yhat.to(device))

    def train_model(self, corpus, epochs, lr=1e-5, epoch_interval=None, batch_size=32):
        if epoch_interval is None:
            epoch_interval = 100

        optim = torch.optim.AdamW(self.parameters(), lr=lr)
        for e in tqdm(range(epochs)):
            X, yhat = self.__fetch_batch(corpus)

            self.train()
            logits, loss = self(X, yhat)
            optim.zero_grad(set_to_none=True)
            loss.backward()
            optim.step()

            if e % epoch_interval == 0:
                loss_type = {}
                self.eval()
                for split in ["train", "test"]:
                    losses = torch.zeros(epoch_interval)
                    for e in range(epoch_interval):
                        X, yhat = self.__fetch_batch(corpus)
                        logits, loss = self(X, yhat)
                        losses[e] = loss.item()
                    loss_type[split] = losses.mean()
                print(
                    f"[ Epoch: {e}, Train Loss: {loss_type['train']}, Test Loss: {loss_type['test']} ]"
                )
