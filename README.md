```
$ python3 train.py --help

usage: train.py [-h] [--n_head N_HEAD] [--n_embd N_EMBD] [--n_layers N_LAYERS] [--block_size BLOCK_SIZE]
                [--batch_size BATCH_SIZE] [--epochs EPOCHS] [--lr LR]

hyperparams for model

options:
  -h, --help            show this help message and exit
  --n_head N_HEAD       number of individual attention-heads
  --n_embd N_EMBD       size of embeddings
  --n_layers N_LAYERS   number of transformer layers
  --block_size BLOCK_SIZE
                        maximum context length
  --batch_size BATCH_SIZE
                        size of batch
  --epochs EPOCHS       number of epochs
  --lr LR               learning rate
```


```
python3 train.py --n_head 16 --n_embd 128 --n_layers 16 --block_size 64 --batch_size 32 --epochs 10000 --lr 0.0001
```
