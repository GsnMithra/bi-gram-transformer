import argparse
from typing import Tuple

import torch
from model import Transformer
from tokenizer import Tokenizer

device = torch.device("mps" if torch.backends.mps.is_available() else "cpu")
parser = argparse.ArgumentParser(description="hyperparams for model")
parser.add_argument(
    "--n_head", type=int, default=16, help="number of individual attention-heads"
)
parser.add_argument("--n_embd", type=int, default=128, help="size of embeddings")
parser.add_argument(
    "--n_layers", type=int, default=16, help="number of transformer layers"
)
parser.add_argument("--block_size", type=int, default=64, help="maximum context length")
parser.add_argument("--batch_size", type=int, default=32, help="size of batch")
parser.add_argument("--epochs", type=int, default=10000, help="number of epochs")
parser.add_argument("--lr", type=float, default=1e-5, help="learning rate")
args = parser.parse_args()


def train_test_split(
    corpus: torch.Tensor, train_size=0.80
) -> Tuple[torch.Tensor, torch.Tensor]:
    N = int(len(corpus) * train_size)
    return (corpus[:N].to(device=device), corpus[N:].to(device=device))


with open("corpus.txt", "r") as f:
    corpus = f.read()

# hyperparams
n_head = args.n_head
n_embd = args.n_embd
n_layers = args.n_layers
block_size = args.block_size
batch_size = args.batch_size

tok = Tokenizer(corpus=corpus)
corpus = tok.encode(corpus).to(device)
vocab_size = len(tok)

train_corpus, test_corpus = train_test_split(corpus)

model = Transformer(vocab_size, n_embd, n_head, n_layers, block_size, batch_size)
model.train_model(corpus, epochs=args.epochs, lr=args.lr)
