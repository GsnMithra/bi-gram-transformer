import torch

device = torch.device("mps" if torch.backends.mps.is_available() else "cpu")


class Tokenizer:
    def __init__(self, corpus):
        self.vocab = sorted(list(set(corpus)))
        self.stoi = {c: i for i, c in enumerate(self.vocab)}
        self.itos = {i: c for i, c in enumerate(self.vocab)}

    def __len__(self) -> int:
        return len(self.vocab)

    def encode(self, string: str) -> torch.Tensor:
        tmp_tensor = list()
        for c in string:
            tmp_tensor.append(self.stoi[c])
        out = torch.tensor(tmp_tensor, dtype=torch.long, device=device)
        return out

    def decode(self, tensor: torch.Tensor) -> str:
        string = ""
        for v in tensor:
            string += self.itos[v]
        return string
